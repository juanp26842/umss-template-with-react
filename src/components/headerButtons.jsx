import React, { Component } from 'react';

class HeaderButtons extends Component {
  state = {
    buttons: [
      { id: 1, value: "UMSS" },
      { id: 2, value: "Facultades" },
      { id: 3, value: "Investigación" },
      { id: 4, value: "Interaccion" },
      { id: 5, value: "Servicios en Linea" },
      { id: 6, value: "Interaccion" },

    ]
  }

  changeName = (id) => {
    console.log(id)
    this.setState({
      buttons: this.state.buttons.map(button => {
        if (button.id === id) {
          return { id: button.id, value: "usado" }
        }
        return button
      })
    })
  }

  render() {
    return (
      <div>
        {Array.from(this.state.buttons).map(button => (
          <button
            className='Header-button'
            onClick={() => this.changeName(button.id)}
            key={button.id}>{button.value} {console.log("me acabo de renderizar")}
          </button>
        ))}
      </div>
    );
  }
}

export default HeaderButtons;

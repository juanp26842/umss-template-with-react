import './coverPhoto.css'

function CoverPhoto() {
  return <div className='cover-background cover-container'>
    <h2 className='cover-text'>#UmssBolOficial</h2>
  </div>
}

export default CoverPhoto

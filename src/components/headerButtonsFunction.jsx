import SingleHeaderButton from './singleHeaderButton';

const buttonTexts = [
  "UMSS",
  "Facultades",
  "Investigación",
  "Interaccion",
  "Servicios en Linea",
  "Contacto"
]

function HeaderButtonsFunciton() {
  return <div>
    {buttonTexts.map((text, index) => {
      return <SingleHeaderButton content={text} key={index} />
    })}
  </div>
}

export default HeaderButtonsFunciton

import CoverPhoto from "./coverPhoto"
import HeaderFunction from "./headerFunction"
import Footer from "./Footer/footer"
import InfoSection from "./infoSection"

function HomePage() {
  return <div>
    <HeaderFunction />
    <CoverPhoto />
    <InfoSection />
    <Footer />
  </div>
}

export default HomePage

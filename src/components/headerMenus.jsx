import './headerMenus.css'

const ummsUrl = "https://www.umss.edu.bo"
const contactUrl = "https://www.umss.edu.bo/contacto/"
const menuContents = [
  {
    title: "UMSS",
    content: [[{
      text: "Nuestra Universidad"
    }, {
      text: "Principios, Fines y Objetivos",
      link: "https://www.umss.edu.bo/principios-fines-y-objetivos/"
    }, {
      text: "Galería de Cancelarios y Rectores",
      link: "https://www.umss.edu.bo/galeria-de-cancelarios-y-rectores/"
    }, {
      text: "Estructura Orgánica",
      link: "https://www.umss.edu.bo/estructura-organica/"
    }, {
      text: "Estatuto Orgánico",
      link: "https://www.umss.edu.bo/wp-content/uploads/2019/12/Estatuto-organico-de-la-UMSS.pdf"
    }, {
      text: "Congreso Universitario",
      link: "https://www.umss.edu.bo/congreso-universitario/"
    }, {
      text: "Auditoría Interna",
      link: "https://www.umss.edu.bo/auditoria/"
    }, {
      text: "Resoluciones",
      link: "https://www.umss.edu.bo/resoluciones/"
    }, {
      text: "Imagen y Contenido en Medios Digitales",
      link: "https://www.umss.edu.bo/index.php/la-universidad-mayor-de-san-simon-redisena-su-marca-digital/"
    }, {
      text: "Registros e Inscripciones",
      link: "http://www.drei.umss.edu.bo"
    }, {
      text: "Trámites",
      link: "https://tramites.umss.edu.bo"
    }, {
      text: "Archivo Central",
      link: "http://www.archivos.umss.edu.bo/webArchivos/"
    }, {
      text: "Plan de Continencia y Protocolo de Bioseguridad contra el COVID 19",
      link: "https://www.umss.edu.bo/plan-de-continencia-y-protocolo-de-bioseguridad-contra-el-covid-19/"
    }
    ],
    [{
      text: "Direcciones Universitarias",
      link: ""
    }, {
      text: "Dirección de Investigación Científica y Tecnológica",
      link: "https://www.dicyt.umss.edu.bo"
    }, {
      text: "Dirección de Interacción Social Universitaria",
      link: "https://disu.umss.edu.bo"
    }, {
      text: "Dirección de Planificación Académica",
      link: "http://www.dpa.umss.edu.bo/inicio/"
    }, {
      text: "Dirección de Planificación, Proyectos y Sistemas",
      link: "https://www.umss.edu.bo/direccion-de-planificacion-proyectos-y-sistemas/"
    }, {
      text: "Dirección de Relaciones Internacionales y Convenios",
      link: "https://www.umss.edu.bo/direccion-de-relaciones-internacionales-y-convenios/"
    }, {
      text: "Dirección Universitaria de Bienestar Estudiantil",
      link: "https://dube.umss.edu.bo"
    }, {
      text: "Dirección Universitaria de Evaluación y Acreditación",
      link: "https://www.umss.edu.bo/direccion-universitaria-de-evaluacion-y-acreditacion/"
    }, {
      text: "Escuela Universitaria de Posgrado",
      link: "https://www.umss.edu.bo/escuela-universitaria-de-posgrado/"
    }
    ]]
  },
  {
    title: "Facultades",
    content: [[{
      text: "Campus Central “Las Cuadras”"
    }, {
      text: "Fac. Humanidades Y Cs. De Educacion",
      link: ""
    }, {
      text: "Fac. de Ciencias Jurídicas y Políticas",
      link: ""
    }, {
      text: "Fac. Arquitectura y Ciencias Del Hábitat",
      link: ""
    }, {
      text: "Fac. Ciencias Económicas",
      link: ""
    }, {
      text: "Fac. de Ciencias y Tecnología",
      link: ""
    }, {
      text: "Fac. Ciencias Sociales",
      link: ""
    }],
    [{
      text: "Campus Salud"
    }, {
      text: "Fac. Medicina",
      link: ""
    }, {
      text: "Fac. Odontología",
      link: ""
    }, {
      text: "Fac. Cs. Farmacéuticas Y Bioquímicas",
      link: ""
    }],
    [{
      text: "Edificio Polifuncional “Casco Histórico”"
    }, {
      text: "Fac. Enfermería",
      link: ""
    }],
    [{
      text: "Campus Tamborada"
    }, {
      text: "Fac. Ciencias Agrícolas Y Pecuarias",
      link: ""
    }, {
      text: "Fac. Desarrollo Rural y Territorial",
      link: ""
    }
    ], [
      {
        text: "Campus Temporal"
      }, {
        text: "Escuela Técnica Forestal",
        link: ""
      }
    ], [
      {
        text: "Campus Quillacollo"
      }, {
        text: "Fac. Ciencias Veterinarias",
        link: ""
      }
    ], [
      {
        text: "Campus Punata"
      }, {
        text: "Fac. Politécnica del Valle Alto",
        link: ""
      }
    ]
    ]
  },
  {
    title: "Investigación",
    link: "http://www.dicyt.umss.edu.bo",
    content: [[
      {
        text: "Recursos Científicos"
      }, {
        text: "Editorial EBSCO",
        link: ""
      },
      {
        text: "Web of Science",
        link: ""
      }, {
        text: "Proyectos de Investigación",
        link: ""
      }, {
        text: "Investigadores",
        link: ""
      }, {
        text: "Centros de Investigación",
        link: ""
      }
    ], [
      {
        text: "Bibliotecas Virtuales",
        link: ""
      }, {
        text: "Recursos",
        link: ""
      }
    ]]
  }, {
    title: "Interacción",
    link: "",
    content: [[
      {
        text: "Canal Universitario TvU",
        link: ""
      }
    ], [
      {
        text: "Tiempo Universitario",
        link: ""
      }
    ], [
      {
        text: "Ballet Universitario",
        link: ""
      }
    ], [
      {
        text: "Coro Universitario",
        link: ""
      }
    ]]
  }, {
    title: "Servicios en Linea",
    content: [[
      {
        text: "Websis",
        link: ""
      }
    ], [
      {
        text: "Correo",
        link: ""
      }
    ], [
      {
        text: "Sitra",
        link: ""
      }
    ], [
      {
        text: "UMSS Stat",
        link: ""
      }
    ]]
  }
]

function HeaderMenus() {
  return <nav>
    <ul className="header-list">
      <a className='Header-button home-button' href={ummsUrl}>Inicio</a>
      {menuContents.map((menu) => {
        return <li>
          <div className='menu'>
            <a className='menu-title Header-button' href={menu.link}>{menu.title}</a>
            <ul className='menu-dropdown'>
              {menu.content?.map((menuList) => {
                return <ul className='list'>
                  {menuList.map((contentRow, index) => {
                    return <li className='hover-effect'>
                      <a className={` ${(index === 0) ? "column-title" : "list-element-text"}`} href={contentRow.link} target='blank'>{contentRow.text}</a>
                    </li>
                  })}
                </ul>
              })}
            </ul>
          </div>
        </li>
      })}
      <a className='Header-button contact-button' href={contactUrl}>Contacto</a>
    </ul>
  </nav>
}

export default HeaderMenus;

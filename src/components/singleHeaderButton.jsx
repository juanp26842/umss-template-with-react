import React, { useState } from 'react';
import './headerButtons.css'

function SingleHeaderButton(props) {
  const [content, setContent] = useState(props.content)
  const originalContent = props.content
  return <button onClick={changeContent}
    className='Header-button'>
    {content}
  </button>

  function changeContent() {
    if (content === originalContent) {
      setContent("Usado")
    } else {
      setContent(originalContent)
    }
  }
}


export default SingleHeaderButton

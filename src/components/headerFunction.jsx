import Socials from './socialsFunction'
import HeaderMenus from './headerMenus';
import UMSS_logo from '../assets/UMSS_logo.png'
import './mainHeader.css'

function HeaderFunction() {

  return <header className='Main-header Header-background'>
    <div>
      <div>
        <img src={UMSS_logo} alt="logo" className='UMSS-logo' />
        <Socials />
      </div>
    </div>
    <HeaderMenus />
  </header>

}

export default HeaderFunction

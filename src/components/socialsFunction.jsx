import './socials.css'

const socialNetworks = [
  { link: "https://www.facebook.com/UmssBolOficial/", networkName: "Facebook" },
  { link: "https://t.me/UmssBolOficial", networkName: "Telegram" },
  { link: "https://www.linkedin.com/school/universidad-mayor-de-san-simon/", networkName: "Linkedin" },
  { link: "https://www.instagram.com/umssboloficial/", networkName: "Instagram" },
  { link: "https://twitter.com/UmssBolOficial", networkName: "Twitter" },
  { link: "https://www.youtube.com/c/UniversidadMayordeSanSimonOficial", networkName: "Youtube" }
]


function Socials() {
  return <div className="socials-container">
    <span className='socials-header'>Visitanos en nuestras RRSS:</span>
    <br />
    {socialNetworks.map((networkInfo, index) => {
      return <a key={index} href={networkInfo.link} className={`social-ref ${networkInfo.networkName.toLowerCase()}-social-ref`} title={`Seguir en ${networkInfo.networkName}`} target='blank' />
    })}
  </div>
}

export default Socials

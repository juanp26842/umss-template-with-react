import React from 'react';
import './footer.css';
import Carousel from '../Carousel/carousel';

function Footer (){
  return(
    <footer className='pie-de-pagina'>
      <div className='contenido-footer'>
        <div className='seccion cero fondo'>
          <div className='contenedor-cero'>
            <div className='fila'>
              <div className='columna'>
                <div className='carousel color-letra carousel-background-color'>
                  <Carousel/>
                </div>
              </div>
              <div className='columna dos'>
                <div className='contenedor-columna-2'>
                  <img className='imagen'
                  src='https://www.umss.edu.bo/wp-content/uploads/2021/08/M.png'
                  data-src='https://www.umss.edu.bo/wp-content/uploads/2021/08/M.png'
                  alt='UMSSBOLOFICIAL'
                  width="90"/>
                  <div className='name-UMSS'>
                    <strong> Universidad Mayor de San Simón </strong>
                  </div>
                </div>
              </div>
              <div className='columna tres'>
                <div className='contenedor-texto'>
                  <p className='name-UMSS'>Visitanos en nuestras RRSS</p>
                </div>
                <ul className='lista-redes'>
                  <li className='redes-sociales social-icon fondo-facebook icon-facebook'>
                    <a href='https://www.facebook.com/UmssBolOficial/' className='icon' title='Seguir en Facebook' target='_blank'>
                      <span className='redes-sociales-seguir' aria-hidden='true'>Seguir</span>
                    </a>
                  </li>
                  <li className='redes-sociales social-icon fondo-twitter icon-twitter'>
                    <a href='https://twitter.com/UmssBolOficial' className='icon' title='Seguir en Twitter' target='_blank'>
                      <span className='redes-sociales-seguir' aria-hidden='true'>Seguir</span>
                    </a>
                  </li>
                  <li className='redes-sociales social-icon fondo-instagram icon-instagram'>
                    <a href='https://www.instagram.com/umssboloficial/' className='icon' title='Seguir en Instagram' target='_blank'>
                      <span className='redes-sociales-seguir' aria-hidden='true'>Seguir</span>
                    </a>
                  </li>
                  <li className='redes-sociales social-icon fondo-in icon-in'>
                    <a href='https://www.linkedin.com/school/universidad-mayor-de-san-simon/' className='icon' title='Seguir en LinkedIn' target='_blank'>
                      <span className='redes-sociales-seguir' aria-hidden='true'>Seguir</span>
                    </a>
                  </li>
                  <li className='redes-sociales social-icon fondo-youtube icon-youtube'>
                    <a href='https://www.youtube.com/c/UniversidadMayordeSanSimonOficial' className='icon' title='Seguir en Youtube' target='_blank'>
                      <span className='redes-sociales-seguir' aria-hidden='true'>Seguir</span>
                    </a>
                  </li>
                  <li className='redes-sociales social-icon fondo-telegram icon-telegram redes-sociales-tg'>
                    <a href='https://t.me/UmssBolOficial' className='icon' title='Seguir en Telegram' target='_blank'>
                      
                      <span className='redes-sociales-seguir' aria-hidden='true'>Seguir</span>
                    </a>
                  </li>
                </ul>
              </div>

            </div>
          </div>
        </div>
        <div className='seccion dos fondo fondo-dos'>
          <div className='fila-dos-dos'>
            <div className='texto-seccion-2 texto-umss'>
              <div className='formato-texto'>
                <p className='text-seccion-dos' style={{textAlign: "center"}}>
                  <span>DERECHOS RESERVADOS © 2022 · </span> 
                  <span style={{textDecoration: "underline"}}>
                    <a style={{color:"#ffffff"}} href='https://www.umss.edu.bo/'>
                    UNIVERSIDAD MAYOR DE SAN SIMÓN
                    </a>
                  </span> 
                </p>
              </div>
            </div>
          </div>
              
        </div>
      </div>
    </footer>
  );
}
export default Footer;
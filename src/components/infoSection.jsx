import './infoSection.css'

function InfoSection() {
  return <div className='main-info-section-container'>
    <div className='info-section-header-container'>
      <div className='info-section-title-container'>
        <h1 className='info-section-header'>Universidad Mayor de San Simón</h1>
      </div>
      <form className='search-form-container'>
        <input className='info-search-box' type="text" placeholder='Buscar' />
        <input className='info-search-button' type="submit" value="Búsqueda" />
      </form>
    </div>
    <div className='info-section-body-container'>
      <iframe width="560" height="300" src="https://www.youtube.com/embed/U0B4j7xCCak" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
      <div style={{ display: "flex", gap: "10px" }}>
        <div style={{ display: "flex", flexDirection: "column", gap: "10px" }}>
          <button>asdasd1</button>
          <button>asdasd2</button>
        </div>
        <div style={{ display: "flex", flexDirection: "column", gap: "10px" }}>
          <button>asdasd3</button>
          <button>asdasd4</button>
        </div>
        <div style={{ display: "flex", flexDirection: "column", gap: "10px" }}>
          <button>asdasd5</button>
          <button>asdasd6</button>
        </div>
      </div>
    </div>
  </div>
}

export default InfoSection
